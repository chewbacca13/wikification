import json
import os, io
import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tag import pos_tag
from nltk.corpus import stopwords
import re
import string
from http.server import HTTPServer, BaseHTTPRequestHandler

root = 'F:\\Labor\\NLP-wiki\\static\\'

_lemmatizer = WordNetLemmatizer()
stopwords = stopwords.words('english')

def create_html(title):
    f = open('static/' + title + '.html', 'w', encoding='utf-8')
    f.write('<!DOCTYPE html>\n <html>\n <body>\n')
    f.write('<h1>'+title+'</h1>\n')
    return f

def write_html(f, text, href=None):
    if href:
        f.write('<p><a href='+href+'>' + str(text) + '</a></p>')
    else:
        f.write('<p>' + str(text) + '</p>')

def close_html(f):
    f.write('</body>\n</html>')
    f.close()

def create_index():
    f_index = create_html('index')

    for subdir, dirs, files in os.walk(root):
        # print('D:',subdir.replace(root, ''))
        sub_i = subdir.replace(root, '')
        if sub_i == '':
            continue

        write_html(f_index, sub_i, sub_i + '\\' + sub_i + '_index.html')

        f_sub = create_html(sub_i + '\\' + sub_i + '_index')
        for file in files:
            # print('F:', file)
            write_html(f_sub, file.replace('.html', ''), file.replace('.html', ''))
        close_html(f_sub)
    close_html(f_index)

fp = open('idf_dict.json')
df_dict = json.loads(fp.read())
fp.close()

fp = open('Keyphrasness_dict.json')
keyph_dict = json.loads(fp.read())
fp.close()



_dict = dict()



for key, value in keyph_dict.items():
    key_tok = key.split(' ')
    avg = 0
    n = 0
    for k in key_tok:
        if k not in stopwords:
            if k in df_dict:
                avg += df_dict[k]
                n += 1
    if n == 0:
        pass
    else:
        _dict[key] = value / (avg/n)

def remove_noise(tokens, stop_words=()):

    cleaned_tokens = []
    tokens = [t for t in tokens if t != '']
    for token, tag in pos_tag(tokens):
        # token = re.sub('http[s]?:\/\/(?:[a-zA-Z]|[0-9]|[$-_@.&+#]|[!*\(\),]|'
        #             '(?:%[0-9a-fA-F][0-9a-fA-F]))+|===.*===|\[\[.*\]\]|<.*>', '', token)
        token = re.sub('<.*>', '' ,token)

        if tag.startswith("NN"):
            pos = 'n'
        elif tag.startswith('VB'):
            pos = 'v'
        else:
           pos = 'a'

        token = _lemmatizer.lemmatize(token, pos)

        # if len(token) > 0 and token not in string.punctuation and token.lower() not in stop_words:
        cleaned_tokens.append(token.lower())
    return cleaned_tokens


n = int(len(_dict) * 0.13)
print(n)
del _dict['html']
del _dict['|c']
_dict = { k: _dict[k] for k in sorted(_dict, key=_dict.get, reverse=True)[:n]}
keywords = [ k for k in _dict.keys() if k not in stopwords ]

# create_index()
# print(keywords[:100])


index_dict = dict()
for key, value in _dict.items():
    keys = remove_noise(key.split(' '))
    for k in keys:
        if k not in stopwords:
            index_dict[k] = key

print('ready')

def create_wiki_link(s):
    return 'https://en.wikipedia.org/wiki/' + s.replace(' ', '_')

def wikify(text):

    raw_tokens = nltk.tokenize.casual_tokenize(text.replace('"',''))
    tokens = remove_noise(raw_tokens)
    keywords_copy = list(index_dict.keys()).copy()

    for i, word in enumerate(tokens):
        if word in keywords_copy:
            s = ' <a href="' + create_wiki_link(index_dict[word]) + '">'+ raw_tokens[i] + '</a> '
            text = text.replace(' '+raw_tokens[i] + ' ', s)
            keywords_copy.remove(word)
            # print(text)
            # raw_tokens[i] = s
    return text

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_GET(self):
        print(self.path)
        if self.path == '/':
            self.path = '/index.html'
            return SimpleHTTPRequestHandler.do_GET(self)
        if self.path == '/favicon.ico':
            return

        if re.match('.*index\.html', self.path):
            f = open('static' + self.path, 'rb')
            self.send_response(200)
            self.end_headers()
            self.wfile.write(f.read())
            f.close()
        else:
            f = io.open('static' + self.path + '.html', 'r', encoding='utf-8')
            self.send_response(200)
            self.end_headers()
            line = ' '
            while line != b'':
                line = f.readline()
                line2 = wikify(line)
                self.wfile.write(line2.encode())
            
            f.close()


httpd = HTTPServer(('localhost', 8080), SimpleHTTPRequestHandler)
httpd.serve_forever()

