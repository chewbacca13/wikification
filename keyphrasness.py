from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tag import pos_tag
from nltk.corpus import stopwords
import re
import string
import json


class Keyphrasness():

    def __init__(self):
        self._dict = dict()
        self._lemmatizer = WordNetLemmatizer()
        self.stopwords = stopwords.words('english')

    def process_text(self, article):
        words = self.remove_noise(re.findall('\[\[([^[][A-Za-z ]+)\]\]', article))
        for w in words:
            if w in self._dict:
                self._dict[w] += 1
            else:
                self._dict[w] = 1

    def dump_dict(self):
        try:
            os.remove('Keyphrasness_dict.json')
        except:
            pass
        with open('Keyphrasness_dict.json', 'w') as fp:
            json.dump(self._dict, fp)
            print('dict dumped')

    def remove_noise(self, tokens, stop_words=()):

        cleaned_tokens = []
        for token, tag in pos_tag(tokens):
            if tag.startswith("NN"):
                pos = 'n'
            elif tag.startswith('VB'):
                pos = 'v'
            else:
               pos = 'a'

            token = self._lemmatizer.lemmatize(token, pos)

            if len(token) > 0 and token not in string.punctuation and token.lower() not in stop_words:
                cleaned_tokens.append(token.lower())
        return cleaned_tokens

