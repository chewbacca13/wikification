import nltk
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.tag import pos_tag
from nltk.corpus import stopwords
import re
import string
import threading


class Tfidf:


    tf_dict = dict()
    idf_dict = dict()

    def __init__(self):
        # self._lock = threading.Lock()
        self._stopwords = stopwords.words('english')
        self._lemmatizer = WordNetLemmatizer()

    def remove_noise(self, tokens, stop_words=()):

        cleaned_tokens = []
        for token, tag in pos_tag(tokens):
            token = re.sub('http[s]?:\/\/(?:[a-zA-Z]|[0-9]|[$-_@.&+#]|[!*\(\),]|'
                       '(?:%[0-9a-fA-F][0-9a-fA-F]))+|===.*===|\[\[.*\]\]|<.*>', '', token)

            if tag.startswith("NN"):
                pos = 'n'
            elif tag.startswith('VB'):
                pos = 'v'
            else:
               pos = 'a'

            token = self._lemmatizer.lemmatize(token, pos)

            if len(token) > 0 and token not in string.punctuation and token.lower() not in stop_words:
                cleaned_tokens.append(token.lower())
        return cleaned_tokens


    def process_article(self, article):

        if not article:
            return
        if len(article) < 100:
            return

        tk = nltk.tokenize.casual_tokenize(article)
        acc = dict()

        tokens = self.remove_noise(tk[:1000], self._stopwords)
        for token in tokens:
            acc[token] = 1
            if token in self.tf_dict:
                self.tf_dict[token] += 1
            else:
                self.tf_dict[token] = 1

        self.new_document(acc)


    def new_document(self, acc_dict):

        # with self._lock:
        for key, value in acc_dict.items():
            if key in self.idf_dict:
                self.idf_dict[key] += 1
            else:
                self.idf_dict[key] = 1
        acc_dict.clear()
